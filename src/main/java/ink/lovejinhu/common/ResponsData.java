package ink.lovejinhu.common;

/**
 * @author jinhu
 * created 2021-08-18 16:52
 */
public class ResponsData {
    public String token;
    public String code;
    public String msg;
    public Object data;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
