package ink.lovejinhu.controller;

import ink.lovejinhu.common.ResponsData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author jinhu
 * created 2021-08-18 16:52
 */
@Controller
public class UserController {
    @GetMapping
    @ResponseBody
    public ResponsData getUsers(String token) {
        ResponsData responsData = new ResponsData();
        responsData.setCode("200");
        responsData.setToken("jijnhu");
        if (token.equals("1")) {
            return responsData;
        } else {
            return new ResponsData();
        }
    }

}
