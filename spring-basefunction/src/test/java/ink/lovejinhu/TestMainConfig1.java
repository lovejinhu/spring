package ink.lovejinhu;

import ink.lovejinhu.config.MainConfig_01;
import ink.lovejinhu.config.MainConfig_02;
import ink.lovejinhu.dao.Dog;
import ink.lovejinhu.dao.Person;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Author: jin
 * @ClassName: Person
 * @Date: 2021/8/20 9:18
 * @Version: V1.0
 **/
public class TestMainConfig1 {
    @Test
    public void test01() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfig_01.class);
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (int i = 0; i < beanDefinitionNames.length; i++) {
            System.out.println(beanDefinitionNames[i]);
        }
    }

    @Test
    public void test02() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfig_02.class);
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        for (int i = 0; i < beanDefinitionNames.length; i++) {
            System.out.println(beanDefinitionNames[i]);
        }
        System.out.println("--------------------------");
        Person bean = applicationContext.getBean(Person.class);
        System.out.println(bean);
        Dog bean1 = applicationContext.getBean(Dog.class);
        System.out.println(bean1);
        Dog bean2 = applicationContext.getBean(Dog.class);
        System.out.println(bean2);

    }
}
