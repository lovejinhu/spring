package ink.lovejinhu.dao;

import org.springframework.stereotype.Repository;

/**
 * @Author: jin
 * @ClassName: Person
 * @Date: 2021/8/19 17:15
 * @Description: //TODO 实体类
 * @Version: V1.0
 **/
@Repository
public class Person {

    public String name;
    public String age;

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }

    public Person(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
