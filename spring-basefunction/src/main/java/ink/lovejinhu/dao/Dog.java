package ink.lovejinhu.dao;

import org.springframework.stereotype.Repository;

/**
 * @Author: jin
 * @ClassName: Person
 * @Date: 2021/8/20 9:42
 * @Version: V1.0
 **/
@Repository
public class Dog {
    /**
     * 狗主人
     */
    public Person person;
    public String name;
    public int age;

    @Override
    public String toString() {
        return "Dog{" +
                "person=" + person +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public Dog(Person person, String name, int age) {
        this.person = person;
        this.name = name;
        this.age = age;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}
