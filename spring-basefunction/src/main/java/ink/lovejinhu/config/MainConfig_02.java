package ink.lovejinhu.config;

/**
 * @Author: jin
 * @ClassName: Person
 * @Date: 2021/8/20 10:07
 * @Version: V1.0
 **/

import ink.lovejinhu.dao.Dog;
import ink.lovejinhu.dao.Person;
import org.springframework.context.annotation.*;

/**
 * 组件注入
 */
@Configuration

public class MainConfig_02 {
    /**
     * ConfigurableBeanFactory#SCOPE_PROTOTYPE
     * @see ConfigurableBeanFactory#SCOPE_SINGLETON
     * @see org.springframework.web.context.WebApplicationContext#SCOPE_REQUEST  request
     * @see org.springframework.web.context.WebApplicationContext#SCOPE_SESSION     sesssion
     * @return\
     * @Scope:调整作用域
     * prototype：多实例的：ioc容器启动并不会去调用方法创建对象放在容器中。
     * 					每次获取的时候才会调用方法创建对象；
     * singleton：单实例的（默认值）：ioc容器启动会调用方法创建对象放到ioc容器中。
     * 			以后每次获取就是直接从容器（map.get()）中拿，
     * request：同一次请求创建一个实例
     * session：同一个session创建一个实例
     *
     * 懒加载：
     * 		单实例bean：默认在容器启动的时候创建对象；
     * 		懒加载：容器启动不创建对象。第一次使用(获取)Bean创建对象，并初始化；
     *
     */
    /**
     * 默认单例，采用懒汉式还是饿汉式
     *
     * @return
     */
    @Lazy
    @Bean(name = "person")
    public Person getPerson() {
        System.out.println("我被创建了");
        return new Person("jinhu", "26");
    }

//    @Bean(name = "person1")
//    public Person getPerson1() {
//        System.out.println("我被创建了");
//        return new Person("ylz", "23");
//    }

    @Scope(value = "prototype")
    @Bean(name = "dog")
    public Dog getDog(Person person) {
        return new Dog(person, "金毛", (int) (Math.random() * 10));
    }

}
