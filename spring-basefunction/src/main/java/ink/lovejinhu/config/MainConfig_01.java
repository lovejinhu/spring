package ink.lovejinhu.config;

import ink.lovejinhu.dao.Person;
import ink.lovejinhu.service.DogService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 * @Author: jinhu
 * @ClassName: MainConfig_01
 * @Date: 2021/8/19 17:13
 * @Description: //TODO 纯注解加载bean的方式
 * @Version: V1.0
 **/

/**
 * 标注这个是一个spring的配置类
 */
@Configuration
/**
 * 标注这个应该扫描哪些文件
 */
@ComponentScan(value = "ink.lovejinhu",
        includeFilters = {@ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class, Service.class})},
        excludeFilters = {@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {DogService.class})},
        useDefaultFilters = false)
//@ComponentScan  value:指定要扫描的包
//excludeFilters = Filter[] ：指定扫描的时候按照什么规则排除那些组件
//includeFilters = Filter[] ：指定扫描的时候只需要包含哪些组件
//FilterType.ANNOTATION：按照注解
//FilterType.ASSIGNABLE_TYPE：按照给定的类型；
//FilterType.ASPECTJ：使用ASPECTJ表达式
//FilterType.REGEX：使用正则指定
//FilterType.CUSTOM：使用自定义规则
//useDefaultFilters为true时主动扫描{@code @Repository}, {@code @Service}, or {@code @Controller}
public class MainConfig_01 {
    @Bean(name = "person")
    public Person getPerson() {
        return new Person("jinhu", "23");
    }
}
