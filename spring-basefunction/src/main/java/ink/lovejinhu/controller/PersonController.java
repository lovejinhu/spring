package ink.lovejinhu.controller;

import ink.lovejinhu.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * @Author: jin
 * @ClassName: Person
 * @Date: 2021/8/20 9:40
 * @Version: V1.0
 **/
@Controller
public class PersonController {
    @Autowired
    public PersonService personService;
}
