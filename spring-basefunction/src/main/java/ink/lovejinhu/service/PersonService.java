package ink.lovejinhu.service;

import ink.lovejinhu.dao.Person;
import org.springframework.stereotype.Service;

/**
 * @Author: jin
 * @ClassName: Person
 * @Date: 2021/8/20 9:20
 * @Version: V1.0
 **/
@Service
public class PersonService {
    public void printAPerson() {
        System.out.println(new Person("jinhu", "" + Math.random()));
    }

}
