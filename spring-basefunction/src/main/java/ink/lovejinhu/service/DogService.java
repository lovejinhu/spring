package ink.lovejinhu.service;

import org.springframework.stereotype.Service;

/**
 * @Author: jin
 * @ClassName: Person
 * @Date: 2021/8/20 9:58
 * @Version: V1.0
 **/
@Service
public class DogService {
    public void print() {
        System.out.println("i am a dog");
    }
}
